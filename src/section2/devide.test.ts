// toThrow
import { ZeroDivisionError, devide } from "./devide";

describe("devide", () => {
  it("2/2は1", () => {
    expect(devide(2, 2)).toBe(1);
  });
  it("2/0はエラー", () => {
    expect(() => devide(2, 0)).toThrow();
    expect(() => devide(2, 0)).toThrow("Cannot divide by zero");
    expect(() => devide(2, 0)).toThrow(ZeroDivisionError);
  });
});
