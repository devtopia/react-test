export class ZeroDivisionError extends Error {}

export const devide = (a: number, b: number): number => {
  if (b === 0) {
    throw new ZeroDivisionError("Cannot divide by zero");
  }
  return a / b;
};
