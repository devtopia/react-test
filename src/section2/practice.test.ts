import { ShoppingList } from "./practice";

describe("ShoppingList", () => {
  let shoppingList: ShoppingList;

  beforeEach(() => {
    shoppingList = new ShoppingList();
  });

  describe("addItem", () => {
    it("Add a item", () => {
      shoppingList.addItem("apple");
      expect(shoppingList.list).toEqual(["apple"]);
    });
  });

  describe("removeItem", () => {
    it("Remove a item", () => {
      shoppingList.addItem("apple");
      shoppingList.removeItem("apple");
      // expect(shoppingList.list).toEqual([]);
      expect(shoppingList.list).not.toContain("apple");
    });

    it("Throw error if item not found", () => {
      expect(() => {
        shoppingList.removeItem("apple");
      }).toThrow("apple not found");
    });
  });
});
