import { sum } from "./sum";

describe("sum", () => {
  // it("should add two numbers", () => {
  //   expect(sum(1, 2)).toBe(3);
  // });
  //
  // it("1 and -2 should return -1", () => {
  //   expect(sum(1, -2)).toBe(-1);
  // });

  it.each`
    a    | b     | expected
    ${1} | ${2}  | ${3}
    ${2} | ${-2} | ${0}
  `("$a + $b = $expected", ({ a, b, expected }) => {
    expect(sum(a, b)).toBe(expected);
  });
});
