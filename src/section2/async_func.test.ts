import { delay } from "./async_func";

describe("delay", () => {
  it("delay 1sec", async () => {
    const result = await delay("Hello World", 1000);
    expect(result).toBe("Hello World");
  });

  it("Tims is negative", async () => {
    try {
      await delay("Hello World", -1000);
    } catch (error: unknown) {
      if (error instanceof Error) {
        expect(error.message).toBe("Time must be positive");
      }
    }
  });
});
