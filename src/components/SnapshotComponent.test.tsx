import { render } from "@testing-library/react";

import SnapshotComponent from "./SnapshotComponent";

describe("SnapshotComponent", () => {
  it("should render correctly", () => {
    const { container } = render(<SnapshotComponent text="Vue" />);
    expect(container).toMatchSnapshot();
  });
});
