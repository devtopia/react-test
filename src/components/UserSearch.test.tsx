import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import axios from "axios";

import { UserSearch } from "./UserSearch";

jest.mock("axios");
const mockAxios = jest.mocked(axios);
const user = userEvent.setup();

describe("UserSearch", () => {
  beforeEach(() => {
    mockAxios.get.mockReset();
  });

  it("should be submitted using the data entered in the input field", async () => {
    const userInfo = { id: 1, name: "Tom Lee" };
    const resp = { data: userInfo };
    mockAxios.get.mockResolvedValue(resp);
    render(<UserSearch />);
    const input = screen.getByRole("textbox");
    await user.type(input, userInfo.name);
    const button = screen.getByRole("button");
    await user.click(button);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(`/api/users?query=${userInfo.name}`);
  });

  it("should be displayed on the screen data obtained from the API", async () => {
    const userInfo = { id: 1, name: "Tom Lee" };
    const resp = { data: userInfo };
    mockAxios.get.mockResolvedValue(resp);
    render(<UserSearch />);
    const input = screen.getByRole("textbox");
    await user.type(input, userInfo.name);
    const button = screen.getByRole("button");
    await user.click(button);
    await waitFor(() => expect(screen.getByText(userInfo.name)).toBeInTheDocument());
  });
});
