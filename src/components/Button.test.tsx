import { render, screen } from "@testing-library/react";

import Button from "./Button";

describe("Button", () => {
  it("Button should render", () => {
    render(<Button label="Click me" onClick={() => alert("clicked")} />); // <Button label="Click me" onClick={() => alert("clicked")} />);

    const element = screen.getByRole("button");
    expect(element).toBeInTheDocument();
    expect(element).toHaveTextContent("Click me");
  });
});
