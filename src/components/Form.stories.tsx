import { expect } from "@storybook/jest";
import type { Meta, StoryObj } from "@storybook/react";
import { within } from "@storybook/test";
import { userEvent } from "@storybook/test";

import Form from "./Form";

const meta = {
  title: "Form",
  component: Form
} as Meta<typeof Form>;

export default meta;

type Story = StoryObj<typeof Form>;

export const Default: Story = {};

export const Testing: Story = {
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement); // screen objectと同様
    const input = canvas.getByRole("textbox");
    await expect(input).toHaveTextContent("");
    await userEvent.type(input, "Hello");
    await expect(input).toHaveValue("Hello");
    await expect(canvas.getByDisplayValue("Hello")).toBeInTheDocument();
  }
};
