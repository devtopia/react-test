/**
 * 練習：以下のUsersクラスのテストを作成してみましょう
 *
 * ヒント
 * 1. jest.mockを使用してaxiosをモック化してみましょう
 * 2. mockResolvedValueまたはmockImplementationを使用してモック化したaxios.getが期待した結果を返すようにしてみましょう
 */
import axios from "axios";

class Users {
  static async all() {
    const resp = await axios.get("https://jsonplaceholder.typicode.com/users");
    return resp.data;
  }
}

export default Users;
