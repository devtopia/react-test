describe("Test mock", () => {
  it("First mock", () => {
    // jest.fn()
    const mockFunc = jest.fn(() => "Hello mock");
    mockFunc();
    expect(mockFunc).toBeCalled();
    expect(mockFunc).toBeCalledTimes(1);
    expect(mockFunc).toBeCalledWith();
    expect(mockFunc()).toBe("Hello mock");
  });

  it("mockImplementation", () => {
    const mockFunc = jest.fn();
    // あとでmockFuncのcallback関数を定義できる。
    mockFunc.mockImplementation(() => "Hello mock");
    expect(mockFunc()).toBe("Hello mock");
  });
});
