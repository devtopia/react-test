describe("Test mock3", () => {
  it("A mock function was called", () => {
    const mockFunc = jest.fn();
    mockFunc();
    // expect(mockFunc).toBeCalled();
    expect(mockFunc).toHaveBeenCalled();
  });

  it("A mock function was called 2 times", () => {
    const mockFunc = jest.fn();
    mockFunc();
    mockFunc();
    expect(mockFunc).toHaveBeenCalledTimes(2);
  });

  it("A mock function was called with arguments", () => {
    const mockFunc = jest.fn();
    mockFunc(1, 2);
    expect(mockFunc).toHaveBeenCalledWith(1, 2);
  });
});
