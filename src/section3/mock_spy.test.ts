import { Calculator } from "./mock_spy";

describe("Calculator", () => {
  it("add", () => {
    const calculator = new Calculator();
    const addSpy = jest.spyOn(calculator, "add");
    const result = calculator.add(1, 2);
    expect(result).toBe(3);
    expect(addSpy).toHaveBeenCalledTimes(1);
    expect(addSpy).toHaveBeenCalledWith(1, 2);
    // expect(calculator.add(1, 2)).toBe(3);

    // spyを解除する
    addSpy.mockRestore();
  });
});
