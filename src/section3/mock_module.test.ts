import fs from "fs";

import { readFile } from "./mock_module";

// module全体をmock化
// 依存するモジュール全体をモック化することで、その挙動を自在に制御することができる
jest.mock("fs");
const mockFs = jest.mocked(fs);
mockFs.readFileSync.mockReturnValue("readFile");

describe("readFile func", () => {
  it("readFile func return the data", () => {
    const result = readFile("src/section3/mock_module.ts");
    // mockでreadFile funcの戻り値を"readFile"に設定したため、このテストは通る。
    expect(result).toBe("readFile");
    expect(fs.readFileSync).toHaveBeenCalledTimes(1);
  });

  it("Read file", () => {
    const path = "src/section3/mock_module.ts";
    const content = readFile(path);
    expect(content).toContain("readFile");
  });
});
