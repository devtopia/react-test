describe("Test mock2", () => {
  it("Set the return value of mock function", () => {
    const mockFunc = jest.fn();
    mockFunc.mockReturnValue("Hello mock");
    expect(mockFunc()).toBe("Hello mock");
  });

  it("Set the return value of the mock function only once", () => {
    const mockFunc = jest.fn();
    mockFunc.mockReturnValueOnce("Hello mock");
    // 一番最初に呼び出された時のみの返り値、その以後はundefinedを返す
    expect(mockFunc()).toBe("Hello mock");
    expect(mockFunc()).toBe(undefined);
  });

  it("Set the async return value of mock function", async () => {
    const mockFunc = jest.fn();
    // mockFunc.mockReturnValue(Promise.resolve("Hello mock"));
    mockFunc.mockResolvedValue("Hello mock");
    expect(await mockFunc()).toBe("Hello mock");
  });
});
