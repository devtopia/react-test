module.exports = {
  preset: "ts-jest/presets/js-with-ts-esm",
  testEnvironment: "jest-environment-jsdom",
  testMatch: ["<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}", "<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}"],
  // testPathIgnorePatterns: ["<rootDir>/dist", "<rootDir>/node_modules"],
  modulePathIgnorePatterns: ["<rootDir>/(build|docs|node_modules|scripts|dist)"],
  moduleNameMapper: {
    "^.+\\.(css|sass|scss)$": "identity-obj-proxy"
  },
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": ["ts-jest", { useESM: true }],
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/fileTransformer.cjs"
  },
  setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"]
};
