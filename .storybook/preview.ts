import type { Preview } from "@storybook/react";

// 全てのstorybookで適用したいパラメータなどの設定を行う。
const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    }
  }
};

export default preview;
